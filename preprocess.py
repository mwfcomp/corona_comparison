import pandas as pd
from get_protein_data import ProteinLookup
import os

no_name = 'Unnamed'


def fix_merged_headers(df: pd.DataFrame):
    cols = df.columns.to_list()

    # find closest and rename column
    for i, c in enumerate(cols):
        if no_name in c:
            for d in reversed(cols[:i]):
                if no_name not in d:
                    break
            cols[i] = d

    # renumber identical named columns
    for i, c in enumerate(cols):
        num_repeats = 0
        for d in cols[i:]:
            if d == c:
                num_repeats += 1
            else:
                break
        if num_repeats > 1:
            for j in range(0, num_repeats):
                cols[i + j] = cols[i + j] + ('.%d' % (j + 1))

    df.set_axis(cols, axis='columns', inplace=True)


if __name__ == '__main__':
    df : pd.DataFrame = pd.read_excel('protein_info.xlsx')
    for index, row in df.iterrows():
        id = row['ProteinID']
        print('On ID: {}'.format(id))
        if(not isinstance(row['Location'], str) or not isinstance(row['Abbreviation'], str)):
            protein_info = ProteinLookup(id)

            loc = protein_info.location()
            print('   Location: {}'.format(loc))
            df.at[index, 'Location'] = loc

            abbrev = protein_info.short_name()
            print('   Abbreviation: {}'.format(abbrev))
            df.at[index, 'Abbreviation'] = abbrev

            df.to_excel('protein_info.xlsx')
    # df['Location'] = df.apply(lambda x: )

    # df = pd.read_excel("C:\Dropbox\scienceData\corona analysis\shiyao 2021\data.xlsx",
    #                    sheet_name="human corona")
    # fix_merged_headers(df)
    # bg_df = pd.read_excel("C:\Dropbox\scienceData\corona analysis\shiyao 2021\data.xlsx",
    #                       sheet_name="human control")
    # df = df.merge(bg_df,left_on='T: Protein IDs',right_on='T: Majority protein IDs')


