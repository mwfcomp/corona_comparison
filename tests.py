import unittest
import pandas as pd
from ordinal_biased_rank_correlation import costa_rank, spearman_rank
import ordinal_biased_rank_correlation as obrc
from scipy.stats import spearmanr

# def spearman(a, b):
#     return spearmanr(a, b).correlation

# costa_rank = lambda a, b: rank_correlation(a, b, costa_rank_weight_rc)
# spearman_rank = lambda a, b: rank_correlation(a, b, spearman_rc)


class MyTestCase(unittest.TestCase):
    def test_spearman_perfect(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [a ** 2 for a in al]
        sc = spearman_rank(al, bl)
        self.assertAlmostEqual(sc, 1.)

    def test_spearman_opposite(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [-1 * a for a in al]
        sc = spearman_rank(al, bl)
        self.assertAlmostEqual(sc, -1.)

    def test_spearman_changes(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [2, 1, 3, 4, 5, 6]
        cl = [2, 1, 4, 3, 5, 6]
        sc_ab = spearman_rank(al, bl)
        sc_ac = spearman_rank(al, cl)
        self.assertGreater(sc_ab, sc_ac)

    def test_lower_ordinal_no_diff_spearman(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [2, 1, 3, 4, 5, 6]
        cl = [1, 3, 2, 4, 5, 6]
        orc_ab = spearman_rank(al, bl)
        orc_ac = spearman_rank(al, cl)
        self.assertAlmostEqual(orc_ac, orc_ab)

    def test_larger_shift_larger_diff_spearman(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [6, 2, 3, 4, 5, 1]
        cl = [2, 1, 3, 4, 5, 6]
        orc_ab = spearmanr(al, bl).correlation
        orc_ac = spearmanr(al, cl).correlation
        self.assertLess(orc_ab, orc_ac)

    def test_lower_ordinal_more_diff_obrc(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [2, 1, 3, 4, 5, 6]
        cl = [1, 3, 2, 4, 5, 6]
        orc_ab = costa_rank(al, bl)
        orc_ac = costa_rank(al, cl)
        self.assertGreater(orc_ab, orc_ac)

    def test_invertible_obrc(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [2, 1, 3, 4, 5, 6]
        orc_ab = costa_rank(al, bl)
        orc_ba = costa_rank(bl, al)
        self.assertAlmostEqual(orc_ab, orc_ba)

    def test_obrc_perfect(self):
        al = [1, 2, 3, 4, 5, 6]
        bl = [a ** 3 for a in al]
        orc_ab = costa_rank(al, bl)
        self.assertAlmostEqual(orc_ab, 1.)

    def test_obrc_opposite(self):
        al = [1,  2, 3, 4, 5, 6, 7, 8, 9, 10]
        bl = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
        orc_ab = costa_rank(al, bl)
        self.assertAlmostEqual(orc_ab, -1.)

    def test_simple_obrc_equiv_to_spearman(self):
        al = [10,30,20,40,50,60,70,80]
        bl = list(reversed(al))
        cl = [40,50,10,30,20,20,70,80]
        dl = list(reversed(cl))
        all = [al,bl,cl,dl]

        s_equiv = lambda a,b: obrc.importance_biased_spearman(obrc.uniform_relevant,a,b)

        for x in all:
            for y in all:
                sp = spearmanr(x,y).correlation
                ob = s_equiv(x,y)
                self.assertAlmostEqual(sp,ob)

    def test_symmetry_obrc(self):
        al = [10,30,20,40,50,60,70,80]
        bl = list(reversed(al))
        cl = [40,50,10,30,20,90,70,80]
        dl = list(reversed(cl))
        all = [al,bl,cl,dl]

        s_equiv = lambda a,b: obrc.importance_biased_spearman(obrc.first_3_relevant,a,b)

        self.assertAlmostEqual(s_equiv(al,bl))

        for x in all:
            for y in all:
                s1 = s_equiv(x,y)
                s2 = s_equiv(y,x)
                self.assertAlmostEqual(s1,s2)
        # al = [10,20,30,40,50,60,70,80]
        # bl = [60,50,40,30,20,10,5, 2]
        # s_equiv = lambda a,b: obrc.importance_biased_spearman(obrc.first_3_relevant,a,b)
        # s1 = s_equiv(al,bl)
        # s2 = s_equiv(bl,al)

        # self.assertAlmostEqual(s1,s2)
        # self.assertAlmostEqual(s, -1)



    def test_nothing(self):
        al = [60,50,40,30,20,10]
        bl = [10,50,40,30,20,60]

        obrc.importance_biased_spearman(obrc.first_3_relevant,al,bl)

        self.assertEqual(0,0)



if __name__ == '__main__':
    unittest.main()
