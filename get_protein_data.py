import urllib.request
import urllib.error
from bs4 import BeautifulSoup
import logging

uniprot_base_url = 'http://www.uniprot.org/uniprot/'
unknown_protein_msg = 'Unknown protein ID'
unknown_location_msg = 'Unknown location'

#for looking up multiple things
class ProteinLookup:

    def __init__(self, id):
        self.id = id
        self.soup = self._lookup_soup()

    def _lookup_soup(self):
        logging.info('looking up {}'.format(self.id))
        prot_url = get_uniprot_url(self.id)
        with urllib.request.urlopen(prot_url) as file:
            soup = BeautifulSoup(file, 'lxml')
        if soup.entry is None:
            self.soup = None
            # raise ValueError('Unknown Protein ID {}'.format(self.id))
        return soup

    def location(self):
        if self.soup is None:
            return unknown_protein_msg
        locations = self.soup.entry.find_all('subcellularlocation')
        if len(locations) > 0:
            return [l.location.text for l in locations]
        else:
            return unknown_location_msg

    def short_name(self):
        if self.soup is None:
            return unknown_protein_msg
        return self.soup.entry.find_next('name').text

def get_uniprot_url(id):
    return uniprot_base_url + id + '.xml'

if __name__ =='__main__':
    protein_info = ProteinLookup('P01023')
    print(protein_info.short_name())
    print(protein_info.location())
    # print(get_subcellular_info('P12345'))
