from scipy.stats import rankdata
import numpy as np
# https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1467-842X.2005.00413.x

costa_rank = lambda a, b: rank_correlation(a, b, costa_rank_weight_rc)
spearman_rank = lambda a, b: rank_correlation(a, b, spearman_rc)
weighted_rank = lambda a, b: rank_correlation(a, b, weighted_linear_rank)
min_weighted_rank = lambda a, b: rank_correlation(a, b, min_weight_rank)
r4 = lambda a, b: rank_correlation(a, b, rank4)
r5 = lambda a, b: rank_correlation(a, b, rank5)
import math


def spearman_rc(R, Q):
    n = len(R)
    rs = 1 - 6 * sum([(R[i] - Q[i]) ** 2 for i in range(n)]) / (n ** 3 - n)
    return rs


def costa_rank_weight_rc(R, Q):
    n = len(R)
    rs = 1 - (6 * sum([(R[i] - Q[i]) ** 2 * (n - R[i] + 1 + n - Q[i] + 1) for i in range(n)]) / \
              (n ** 4 + n ** 3 - n ** 2 - n))
    return rs


w = 1000


def weighted_linear_rank(R, Q):
    n = len(R)
    rs = 1 - (6 * sum([(R[i] - Q[i]) ** 2 * w * (n - R[i] + 1 + n - Q[i] + 1) for i in range(n)]) /
              (w * n ** 4 + n ** 3 - n ** 2 - n))
    return rs


wp = 4


def weighted_exponential_rank(R, Q):
    n = len(R)
    rs = 1 - (6 * sum([(R[i] - Q[i]) ** 2 * (n - R[i] + 1 + n - Q[i] + 1) ** wp for i in range(n)]) /
              (n * (n + 1) ** wp * (n ** 2 - 1)))
    return rs


def min_weight_rank(R, Q):
    n = len(R)
    max_factor = sum(
        [(4 * i * i - 4 * i * n - 4 * i + n * n + 2 * n + 1) / (min(i, -i + n + 1)) for i in list(range(1, n + 1))])
    rs = 1 - (6 * sum([(R[i] - Q[i]) ** 2 / (min(R[i], Q[i])) for i in range(n)]) / max_factor)
    return rs


def rank4(R, Q):
    n = len(R)
    rs = 1 - 6 * (sum([(R[i] - Q[i]) ** 2 / ((R[i] + Q[i]) / 2) for i in range(n)]) / (2 * n * (n - 1)))
    return rs


def rank5(R, Q):
    n = len(R)
    rs = 1 - (6 * sum([(R[i] - Q[i]) ** 2 / ((R[i] + Q[i]) ** 2 / 2) for i in range(n)]) / (
                2 * n * (n - 1) / ((n + 1))))
    return rs


def rank_correlation(a, b, dst=spearman_rc):
    if len(a) != len(b):
        raise ValueError('lists might have equal length')
    R = rankdata(a)
    Q = rankdata(b)
    rs = dst(R, Q)
    return rs


def importance_biased_spearman(order_to_importance_fn, lista, listb):
    a = rankdata(lista)
    b = rankdata(listb)
    order = range(0, len(a))
    imp = [order_to_importance_fn(i) for i in order]
    imp = [i / sum(imp) for i in imp]
    #### works for no ties
    # ab_imps = [(imp[i], imp[list(b).index(a[i])]) for i in order]
    # comb_imp = [(ai + bi) / 2 for (ai, bi) in ab_imps]
    #### end

    #the closest index in b for each rank in b. This is one way to handle ties
    #maybe can do something better for ties rather than assuming min
    a_b_ind = [np.abs(b-a[i]).argmin() for i in order]

    ofn = order_to_importance_fn
    ab_imps = [(ofn(i), ofn(a_b_ind[i])) for i in order]
    relevance = np.array([max(ai,bi) for (ai, bi) in ab_imps])
    norm_relevance = relevance / np.average(relevance)

    a_bar = sum(a) / len(a)
    b_bar = sum(b) / len(b)
    n_inv = 1 / len(a)
    d = lambda xs, xb: n_inv * sum([(xs[i] - xb) ** 2 for i in order])
    num = sum([(a[i] - a_bar) * (b[i] - b_bar) * norm_relevance[i] for i in order]) * n_inv
    den = math.sqrt(d(a, a_bar) * d(b, b_bar))
    return num / den


def first_3_relevant(order):
    if order < 3:
        return 1
    else:
        return 0


def uniform_relevant(order):
    return 10


def linear_relevant(order, scaling):
    return scaling / order


def obrc(a, b, dropoff_rate=0.8):
    pass
