import seaborn as sns
import numpy as np
import matplotlib as mpl

sample_columns = []
name_column = ""


# assumes heatmap is of log10 data; changes labels according to this
def do_heatmap(d_df):
    # diverge = sns.diverging_palette(10, 150, as_cmap=True, center='dark')
    # diverge = sns.color_palette("icefire", as_cmap=True)
    # diverge = sns.color_palette("vlag", as_cmap=True)
    diverge = sns.diverging_palette(220, 20, center='dark', as_cmap=True)
    # display(to_disp[sample_columns]))
    ax = sns.heatmap(d_df[sample_columns], vmin=-3, vmax=3, cmap=diverge, center=0,
                     cbar_kws={"extend": "both"})
    cbar : mpl = ax.collections[0].colorbar
    ticklabels = ['{}x'.format(pow(10, t)) for t in cbar.get_ticks()]
    cbar.set_ticks(cbar.get_ticks())
    cbar.set_ticklabels(ticklabels)

def _enrichment_and_names(df):
    individual_proteins = df[[name_column] + sample_columns]
    individual_proteins.set_index(name_column, inplace=True)
    return individual_proteins


def heatmap_naive(df, top=0):
    pf = _enrichment_and_names(df)
    to_disp = pf.sort_values(by='C.1', ascending=False)

    to_disp = to_disp.replace([np.inf, -np.inf], np.nan).dropna()
    if top>0:
        to_disp = to_disp[1:top]
    sns.heatmap(to_disp[sample_columns], vmin=0, vmax=0.2)
    # do_heatmap(pf)
    pass

def heatmap_only_common_proteins(df):
    pf = _enrichment_and_names(df)
    to_disp: pd.DataFrame = np.log10(pf)
    to_disp = to_disp.replace([np.inf, -np.inf], np.nan).dropna()

    # mpl.pyplot.figure(figsize=(10, 15))
    do_heatmap(to_disp)
    return to_disp


def heatmap_standin_infinites(df):
    pf = _enrichment_and_names(df)
    to_disp: pd.DataFrame = np.log10(pf)
    to_disp = to_disp.replace(np.nan, 10)
    to_disp = to_disp.replace(np.inf, 10)
    to_disp = to_disp.replace(-np.inf, -10)

    # display(relevant)
    # df[['T: Protein names_x', 'median_enrichment']]
    mpl.pyplot.figure(figsize=(10, 40))
    do_heatmap(to_disp)
    return to_disp
