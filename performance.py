from ordinal_biased_rank_correlation import *
import matplotlib.pyplot as plt

plt.figure()
for name, ranker in ('Costa', costa_rank), \
                    ('Spearman', spearman_rank), \
                    ('r4', r4), \
                    ('r5', r5):
                    # ('linear weight', weighted_linear_rank), \
                    # ('exp weight', weighted_exponential_rank):
    transloc_scores = []
    subs_scores = []

    x = range(50)
    rx = list(reversed(x))
    swaps = range(len(x)-1)
    for i in swaps:
        y = list(range(len(x)))
        z = list(range(len(x)))
        swap = y[0]
        y[0] = y[i]
        y[i] = swap
        transloc_scores.append(ranker(x, y))

        swap = z[i]
        z[i] = z[i+1]
        z[i+1] = swap
        subs_scores.append(ranker(x,z))

    color = next(plt.gca()._get_lines.prop_cycler)['color']
    plt.plot(swaps, transloc_scores, label=name + 'Transloc', color=color)
    plt.plot(swaps, subs_scores, '-o', label=name +'Subsequent', color=color)
    print("Sanity {} Perfect: {}, Inverted: {}".format(name, ranker(x, x), ranker(x, rx)))

    # plt.title(name)
plt.xlabel('Position')
plt.ylabel('')
plt.legend()
plt.show()



